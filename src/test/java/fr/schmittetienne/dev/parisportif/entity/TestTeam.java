package fr.schmittetienne.dev.parisportif.entity;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TestTeam {
    private Team testingTeam;

    @BeforeEach
    void setUp() {
        testingTeam = new Team();
    }

    @AfterEach
    void tearDown() {
        testingTeam = null;
    }

    @Test
    public void testTeamNameIsSame() {
        String teamName = "Strasbourg";
        testingTeam.setName(teamName);

        Assertions.assertSame(this.testingTeam.getName(), teamName);
    }

    @Test
    public void testTeamNameIsNotSame() {
        testingTeam.setName("NameOne");

        Assertions.assertNotSame(this.testingTeam.getName(), "NameTwo");
    }
}

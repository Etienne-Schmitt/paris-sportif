package fr.schmittetienne.dev.parisportif.repository;

import fr.schmittetienne.dev.parisportif.entity.Team;
import org.springframework.data.repository.CrudRepository;

public interface TeamRepository extends CrudRepository<Team, Integer> {
}

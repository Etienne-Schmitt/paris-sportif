package fr.schmittetienne.dev.parisportif.controller;

import fr.schmittetienne.dev.parisportif.entity.Team;
import fr.schmittetienne.dev.parisportif.repository.TeamRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EntityController {
    private final TeamRepository teamRepository;

    EntityController(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    @GetMapping("/allTeam")
    public Iterable<Team> team() {
        return teamRepository.findAll();
    }
}

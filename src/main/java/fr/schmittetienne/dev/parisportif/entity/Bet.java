package fr.schmittetienne.dev.parisportif.entity;

import com.sun.istack.NotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Bet {
    @Id
    @GeneratedValue
    private int id;

    @NotNull
    @Column(nullable = false)
    private int amount;

    @NotNull
    @Column(nullable = false)
    private float oddsWhenPayed;

    @NotNull
    @Column(nullable = false)
    private boolean resolved;

    // TODO Add user relation

    // TODO Add odds relation


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public float getOddsWhenPayed() {
        return oddsWhenPayed;
    }

    public void setOddsWhenPayed(float oddsWhenPayed) {
        this.oddsWhenPayed = oddsWhenPayed;
    }

    public boolean isResolved() {
        return resolved;
    }

    public void setResolved(boolean resolved) {
        this.resolved = resolved;
    }
}

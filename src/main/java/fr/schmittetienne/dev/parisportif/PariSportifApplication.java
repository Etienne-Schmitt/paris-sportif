package fr.schmittetienne.dev.parisportif;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PariSportifApplication {

    public static void main(String[] args) {
        SpringApplication.run(PariSportifApplication.class, args);
    }

}

DROP TABLE IF EXISTS team;
CREATE TABLE team
(
    id           INT AUTO_INCREMENT NOT NULL,
    name         VARCHAR(255)       NOT NULL,
    country_code VARCHAR(255)       NOT NULL,
    description  LONGTEXT DEFAULT NULL,
    PRIMARY KEY (id)
);

INSERT INTO team (name, country_code, description)
VALUES ('PSG', 'FR', null),
       ('OM', 'FR', null),
       ('Jaguar', 'US', null);

DROP TABLE IF EXISTS event;
CREATE TABLE event
(
    id           INT AUTO_INCREMENT NOT NULL,
    sport_id     INT                NOT NULL,
    name         LONGTEXT           NOT NULL,
    event_date   DATETIME           NOT NULL,
    location     LONGTEXT           NOT NULL,
    country_code VARCHAR(255)       NOT NULL,
    time_zone    VARCHAR(255)       NOT NULL,
    description  LONGTEXT           NOT NULL,
    result       VARCHAR(255)       NOT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS event_odds;
CREATE TABLE event_odds
(
    event_id INT NOT NULL,
    odds_id  INT NOT NULL,
    PRIMARY KEY (event_id, odds_id)
);

DROP TABLE IF EXISTS event_competitor;
CREATE TABLE event_competitor
(
    event_id      INT NOT NULL,
    competitor_id INT NOT NULL,
    PRIMARY KEY (event_id, competitor_id)
);

DROP TABLE IF EXISTS wallet;
CREATE TABLE wallet
(
    id      INT AUTO_INCREMENT NOT NULL,
    balance INT                NOT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS wallet_wallet_payment;
CREATE TABLE wallet_wallet_payment
(
    wallet_id         INT NOT NULL,
    wallet_payment_id INT NOT NULL,
    PRIMARY KEY (wallet_id, wallet_payment_id)
);

DROP TABLE IF EXISTS bet_payment;
CREATE TABLE bet_payment
(
    id          INT AUTO_INCREMENT NOT NULL,
    amount      INT                NOT NULL,
    date        DATETIME           NOT NULL,
    description LONGTEXT           NOT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS competitor_team_status;
CREATE TABLE competitor_team_status
(
    id            INT AUTO_INCREMENT NOT NULL,
    competitor_id INT                NOT NULL,
    team_id       INT                NOT NULL,
    status_id     INT                NOT NULL,
    date          DATE               NOT NULL COMMENT '(DC2Type:date_immutable)',
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS bet;
CREATE TABLE bet
(
    id                INT AUTO_INCREMENT NOT NULL,
    odds_reference_id INT                NOT NULL,
    user_id           INT                NOT NULL,
    amount            INT                NOT NULL,
    odds_when_payed   DOUBLE PRECISION   NOT NULL,
    resolved          TINYINT(1) DEFAULT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS user;
CREATE TABLE user
(
    id              INT AUTO_INCREMENT NOT NULL,
    wallet_id       INT                NOT NULL,
    roles           JSON               NOT NULL,
    email           VARCHAR(180)       NOT NULL,
    password        VARCHAR(255)       NOT NULL,
    lastname        VARCHAR(255)       NOT NULL,
    firstname       VARCHAR(255)       NOT NULL,
    birthdate       DATE               NOT NULL COMMENT '(DC2Type:date_immutable)',
    country_code    VARCHAR(255)       NOT NULL,
    time_zone       VARCHAR(255)       NOT NULL,
    creation_date   DATE               NOT NULL COMMENT '(DC2Type:date_immutable)',
    active_since    DATE DEFAULT NULL COMMENT '(DC2Type:date_immutable)',
    suspended_since DATE DEFAULT NULL COMMENT '(DC2Type:date_immutable)',
    deleted_since   DATE DEFAULT NULL COMMENT '(DC2Type:date_immutable)',
    active          TINYINT(1)         NOT NULL,
    suspended       TINYINT(1)         NOT NULL,
    deleted         TINYINT(1)         NOT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS sport;
CREATE TABLE sport
(
    id            INT AUTO_INCREMENT NOT NULL,
    sport_type_id INT                NOT NULL,
    name          VARCHAR(255)       NOT NULL,
    description   VARCHAR(255)       NOT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS wallet_payment;
CREATE TABLE wallet_payment
(
    id             INT AUTO_INCREMENT NOT NULL,
    date           DATETIME           NOT NULL COMMENT '(DC2Type:datetime_immutable)',
    transaction_id VARCHAR(255)       NOT NULL,
    amount         INT                NOT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS odds;
CREATE TABLE odds
(
    id          INT AUTO_INCREMENT NOT NULL,
    description LONGTEXT           NOT NULL,
    odds_value  DOUBLE PRECISION   NOT NULL,
    winning     TINYINT(1) DEFAULT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS status;
CREATE TABLE status
(
    id          INT AUTO_INCREMENT NOT NULL,
    name        VARCHAR(255)       NOT NULL,
    description VARCHAR(255)       NOT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS competition;
CREATE TABLE competition
(
    id                    INT AUTO_INCREMENT NOT NULL,
    parent_competition_id INT DEFAULT NULL,
    name                  LONGTEXT           NOT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS competitor;
CREATE TABLE competitor
(
    id           INT AUTO_INCREMENT NOT NULL,
    firstname    VARCHAR(255)       NOT NULL,
    lastname     VARCHAR(255)       NOT NULL,
    country_code VARCHAR(255)       NOT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS sport_type;
CREATE TABLE sport_type
(
    id          INT AUTO_INCREMENT NOT NULL,
    name        VARCHAR(255)       NOT NULL,
    description VARCHAR(255)       NOT NULL,
    PRIMARY KEY (id)
);